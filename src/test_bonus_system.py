from bonus_system import calculateBonuses


def test_calculate_bonuses_standard():
    assert calculateBonuses("Standard", 5000) == 0.5
    assert calculateBonuses("Standard", 10000) == 0.75
    assert calculateBonuses("Standard", 50000) == 1
    assert calculateBonuses("Standard", 100000) == 1.25
    assert round(calculateBonuses('Standard', 100001), 2) == 1.25


def test_calculate_bonuses_premium():
    assert calculateBonuses("Premium", 5000) == 0.1
    assert round(calculateBonuses("Premium", 10000), 3) == 0.15
    assert calculateBonuses("Premium", 50000) == 0.2
    assert calculateBonuses("Premium", 100000) == 0.25
    assert round(calculateBonuses('Premium', 100001), 2) == 0.25


def test_calculate_bonuses_diamond():
    assert calculateBonuses("Diamond", 5000) == 0.2
    assert round(calculateBonuses("Diamond", 10000), 3) == 0.3
    assert calculateBonuses("Diamond", 50000) == 0.4
    assert calculateBonuses("Diamond", 100000) == 0.5
    assert round(calculateBonuses('Diamond', 100001), 2) == 0.5


def test_calculate_bonuses_other():
    assert calculateBonuses("Other", 5000) == 0
    assert calculateBonuses("Other", 10000) == 0
    assert calculateBonuses("Other", 50000) == 0
    assert calculateBonuses("Other", 100000) == 0


def test_random():
    assert calculateBonuses('A', 10000) == 0
    assert calculateBonuses('Z', 10000) == 0
